package ictgradschool.industry.lab10;

public class program {

    public static void main(String[] args) {

        System.out.println(bad3(-1));

    }

    public static double bar(double x, int n) {
        if (n > 1) {
            return x * bar(x, n - 1);
        } else if (n < 0) {
            return 1.0 / bar(x, -n);
        } else

            return x;

    }

    public static int bad3(int n) {
        if (n == 0) {
            return 0;
        }

        return n + bad3(n + 1);
    }
}
